<?php

/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 29/03/2015
 * Time: 2:12 PM
 */

class ComEntrymarkControllerQuestion extends ComMediumControllerDefault
{

    public function __construct(KConfig $config)
    {
        parent::__construct($config);

        $this->registerCallback('before.post', array($this, '_prepareUpdate'));


    }

    function _initialize(KConfig $config)
    {
        $config->append(array(
            'behaviors' => array('answerable')
        ));

        parent::_initialize($config);

    }

    // also, needs to be in some kind of type filter. Check the stores for anything like that.
    function _prepareUpdate($context)
    {

        if($context->data->params)
        {

            $context->data->params = $context->data->params->toJson();
        }
    }
}