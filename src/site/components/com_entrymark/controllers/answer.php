<?php

/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 3/04/2015
 * Time: 9:18 PM
 */
class ComEntrymarkControllerAnswer extends ComBaseControllerService
{
    public function __construct(KConfig $config)
    {
        parent::__construct($config);

        $this->registerActionAlias('editanswer', 'edit');


    }

    function _initialize(KConfig $config)
    {
        $config->append(array(
            'behaviors' => array('parentable')
        ));

        parent::_initialize($config);
    }

    /**
     * Returns whether a comment can be added
     *
     * @return boolean
     */
    public function canAdd()
    {
        return true;//$this->parent && $this->parent->authorize('access');
    }

    /**
     * Returns whether a comment can be added
     *
     * @return boolean
     */
    public function canEdit()
    {
        return true;
    }

    protected function _actionAdd($context)
    {

        $data = $context->data;
        $body = $data->body;
        $params = $data->params;

        $return = $this->setItem($this->parent->addAnswer($body, $params, $data->email))->getItem();

        return $return;
    }

    protected function _actionEdit($context)
    {

        $data = $context->data;
        $body = $data->body;

        $this->getItem()->body = $body;

        $this->getItem()->params = $data->params;

        $message = JText::_("COM-ENTRYMARK-ANSWER-THANKYOU-CASE" . strtoupper($this->getItem()->parent->case));
        $this->setMessage($message);
        return $this->getItem();

    }
}