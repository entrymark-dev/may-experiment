<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 27/03/2015
 * Time: 5:19 PM
 */

class ComEntrymarkControllerToolbarActorbar extends ComBaseControllerToolbarActorbar
{
    /**
     * Before _actionGet controller event
     *
     * @param  KEvent $event Event object
     *
     * @return string
     */
    public function onBeforeControllerGet(KEvent $event)
    {
        parent::onBeforeControllerGet($event);

        if ( $this->getController()->isOwnable() && $this->getController()->actor )
        {
            $actor = pick($this->getController()->actor, get_viewer());
            $this->setTitle(sprintf(JText::_('COM-ENTRYMARK-HEADER-QUESTION'), $actor->name));
            $this->setActor( $actor );
        }
    }
}