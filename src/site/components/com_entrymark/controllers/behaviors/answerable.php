<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 5/04/2015
 * Time: 7:28 AM
 */

class ComEntrymarkControllerBehaviorAnswerable extends KControllerBehaviorAbstract
{

    protected $_answer_controller;


    /**
     * Adds a answer
     *
     * @param KCommandContext $context
     *
     * @return ComBaseDomainEntityanswer
     */
    protected function _actionAddanswer(KCommandContext $context)
    {


        $data = $context->data;

        $answer = $this->getAnswerController()->add(array('body' => $data->body, 'params'=> $data->params, 'email' => $data->email));


        $context->response->status = KHttpResponse::CREATED;
        $context->answer = $answer;


        if ($this->isDispatched()) {


            $context->response->setRedirect(JRoute::_($answer->parent->getURL()), 'Thanks for your answer');

        }

        $item = $this->getMixer()->getItem();

        $message = JText::_("COM-ENTRYMARK-ANSWER-THANKYOU-UPDATE");
        $this->getMixer()->setMessage($message);
        return $answer;
    }

        /**
     * Returns the answer controller
     *
     * @return ComBaseControlleranswer
     */
    public function getAnswerController()
    {
        if (!isset($this->_answer_controller)) {
            $identifier = clone $this->getIdentifier();
            $identifier->path = array('controller');
            $identifier->name = 'answer';
            $request = new LibBaseControllerRequest(array('format' => $this->getRequest()->getFormat()));

            if ($this->getRequest()->has('get'))
                $request->set('get', $this->getRequest()->get('get'));

            $request->append(pick($this->_mixer->getRequest()->answer, array()));


            $this->_answer_controller = $this->getService($identifier, array(
                'request' => $request,
                'response' => $this->getResponse()
            ));


            //set the parent
            if ($this->getItem())
                $this->_answer_controller->pid($this->getItem()->id);



        }
        return $this->_answer_controller;
    }
}