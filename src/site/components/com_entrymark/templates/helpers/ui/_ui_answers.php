<?php defined( 'KOOWA' ) or die( 'Restricted access' ) ?>

<?php if ( $can_answer ) : ?>
    <?
    $form = array(
        'parent'=>$entity,
        'editor'=>$editor,
        'case' => $case
    );
    if(isset($answer)):
        $form['answer'] = $answer;
        endif;
    ?>
    <?= @view('answer')->load('form', $form)?>
<?php endif;?>
<? /*?>
]<div class="an-answers-wrapper">
<div class="an-answers an-entities">
	<?php foreach($answers as $answer) : ?>
	<?= @view('answer')->answer($answer)->strip_tags($strip_tags)->truncate_body($truncate_body)->editor($editor) ?>
	<?php endforeach; ?>
</div>

<?php if (!empty($pagination)) : ?>
<?= $pagination ?>
<?php endif; ?>

<?php if ( $show_guest_prompt && !$can_answer ) : ?>
    <?php if( $viewer->guest() ) : ?>
        <?= $entity->get('type') ?>
        <?php $return = base64_encode(@route($entity->getURL())); ?>
        
    <?php elseif ( !$entity->openToanswer ) : ?>
        <?= @message(@text('LIB-AN-answers-ARE-CLOSED')) ?>
    <?php elseif ( !empty($require_follow) ) : ?>
       <div class="alert alert-info">
            <p><?= sprintf(@text('LIB-AN-answer-MUST-FOLLOW'), $entity->owner->name) ?></p>
            <p><a class="btn" data-trigger="Submit" href="<?= @route($entity->owner->getURL().'&action=follow') ?>"><?= @text('COM-ACTORS-SOCIALGRAPH-FOLLOW')?></a></p>
       </div>
    <?php else : ?>
        <?= @message(@text('LIB-AN-answer-NO-PERMISSION')) ?>
    <?php endif; ?>    
<?php endif; ?>
</div>
 <? */ ?>