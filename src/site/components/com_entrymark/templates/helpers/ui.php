<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 4/04/2015
 * Time: 2:37 PM
 */

class ComEntrymarkTemplateHelperUi extends ComBaseTemplateHelperUi
{
    protected function _initialize(KConfig $config)
    {
        $path[] = dirname(__FILE__) . '/ui';

        $config->append(array(
            'paths' => $path
        ));

        parent::_initialize($config);
    }

    function answers($question, $config = array() )
    {
        $config = new KConfig($config);

        $config->append(array(
            'can_answer' => true,
            'entity' => $question,
            'answers' => array(),
            'show_guest_prompt' => true,
            'case' => $question->case ? $question->case : 'one' // carrying through of experiment configuration
        ));

        $answer = array();

        /**
         * A restriction piece can go here based on a question param limiting the number of answers/guesses a user can
         *make.
         */
        if(get_viewer()->id /*&& $question->restriction > 0*/)
        {
            $answer = $question->answers->find(
                array('parent_id' => $question->id, 'created_by' => get_viewer()->id)
            );
        }


        if(count($answer))
        {
            $config['answer'] = $answer;
        }

        $data   = $this->getTemplate()->getData();
        $limit  = isset($data['limit'])  ? $data['limit']  : 0;
        $offset = isset($data['start'])  ? $data['start']  : 0;

        //WHEN $question->answers implemented
        if ( !isset($config['answers']) ) {

           // $config['answers'] = $question->answers->order('creationTime', 'ASC')->limit($limit, $offset);
        }

        return $this->_render('answers', $config->toArray());

    }

    function map($config)
    {

        $config = new KConfig($config);
        return $this->_render('mapbox_geocoding', $config->toArray());
    }
}