<?php

/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 15/04/2015
 * Time: 4:01 PM
 */
class ComEntrymarkTemplateHelperSelector extends LibBaseTemplateHelperSelector
{

//    function school($config = array())
//    {
//        $config = new KConfig($config);
//
//
//        $config->append(array(
//            'selected' => null,
//            'prompt' => true
//        ));
//
//        $selected = $config->selected;
//        unset($config->selected);
//        $prompt = $config->prompt;
//
//        if ($prompt) {
//            $options[] = $prompt;
//        }
//
//        $data = $this->getService('repos://site/entrymark.school')->fetchSet();
//
//        foreach ($data as $datum) {
//            $options[$datum->id] = $datum->name;
//        }
//
//
//        return $this->_html->select($config->name, array('options' => $options, 'selected' => $selected), KConfig::unbox($config));
//    }

    function grade($config = array())
    {
        $config = new KConfig($config);


        $config->append(array(
            'selected' => null,
            'prompt' => true,
            'options' => false
        ));

        $selected = $config->selected;
        unset($config->selected);
        $prompt = $config->prompt;

        if ($prompt) {
            $grades[0] = $prompt;
        }
        if (!$config->options) {

            for ($i = 7; $i < 13; $i++) {
                $grades[$i] = "Grade $i";
            }

            $options = $grades;
        }

        return $this->_html->select($config->name, array('options' => $options, 'selected' => $selected), KConfig::unbox($config));
    }

    function role($config = array())
    {
        $config = new KConfig($config);

        foreach($config->options as $option)
        {
            $html[] ="<label>{$option->label}";
            $html[] = $this->_html->radio($config->name, $option->value, $config->selected == $option->value, $config->attribs);
            $html[] = "</label>";
        }

        return implode(PHP_EOL, $html);
    }


    function questioncase($config = array())
    {
        $config = new KConfig($config);

        foreach($config->options as $option)
        {
            $html[] ="<label>{$option->label}";
            $html[] = $this->_html->radio($config->name, $option->value, $config->selected == $option->value, $config->attribs);
            $html[] = "</label>";
        }

        return implode(PHP_EOL, $html);
    }
    function map($config)
    {
        $config = new KConfig($config);
        return '<em>plugin needs to be built</em>';
    }
    function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            $config = new KConfig($args[0]);


            $config->append(array(
                'selected' => null,
                'prompt' => true,
                'entity' => $method
            ));

            $selected = $config->selected;
            unset($config->selected);
            $prompt = $config->prompt;

            if ($prompt) {
                $options[] = $prompt;
            }
            $identifier = clone $this->getIdentifier();
            $identifier->type = 'repos';
            $identifier->path = array();
            $identifier->name = $config->entity;

            $data = $this->getService($identifier)->fetchSet();

            foreach ($data as $datum) {
                $options[$datum->id] = $datum->name;
            }


            return $this->_html->select($config->name, array('options' => $options, 'selected' => $selected), KConfig::unbox($config));
        }
    }

}