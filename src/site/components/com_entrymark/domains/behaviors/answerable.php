<?php

/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 5/04/2015
 * Time: 8:14 AM
 */
class ComEntrymarkDomainBehaviorAnswerable extends AnDomainBehaviorAbstract
{

    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'answer' => array(),
            'relationships' => array(

                'answers' => array('child' => 'answer', 'child_key' => 'parent', 'parent_delete' => 'ignore')
            )
        ));

        parent::_initialize($config);
    }

    public function addAnswer($answer, $params, $email)
    {

        if(empty($params))
        {
            $params = new KConfig();
        }
        JPluginHelper::importPlugin('koowa', null, true, KService::get('anahita:event.dispatcher'));
        if (is_string($answer)) {
            $answer = KHelperString::trim($answer);

            $answer = array(
                'author' => get_viewer(),
                'body' => $answer,
                'component' => $this->component,
                'params' => $params->toJson(),
                'email' => $email
            );
        }

        $answer = $this->_mixer->answers->addNew($answer);

        if ($this->_mixer->isSubscribable() && $answer->author)
            $this->_mixer->addSubscriber($answer->author);


        return $answer;
    }

	public function getResultsBrick()
    {
		/* 	Not sure how to request this through framework
			Doing it old school for short term
		*/
		// Range filters
		$range_low = isset($_REQUEST['range_low']) ? $_REQUEST['range_low'] : NULL;
		$range_high = isset($_REQUEST['range_high']) ? $_REQUEST['range_high'] : NULL;
		if ($range_low === NULL || $range_high === NULL){
			$filter = 0;
		} else {
			$filter = 1;
		}
		// End of Range Filters
		
		$extended = array();
		$ext_base = array('school' => 0, 'teacher' => 0, 'grade' => 0, 'role'=>'other');
		$brick = array();
		$profile = array();
		$raw = array();
		$traw = array();
		$legend = array();
		$legend_count = 0;
		$grouped_profile = array();
		$diversity=array();
		foreach($this->_mixer->answers as $answer)
        {
			$skip = 0;
			if ($filter){
				if ($answer->body <= $range_low || $answer->body >= $range_high) $skip = 1; 
			}
			
			
			if (abs($answer->body)+1 > 1 && !$skip)
			{
                $params = $answer->getAnswerParams()->toArray();
				$extended[$answer->id]=array_merge($ext_base, $params);
				if ($extended[$answer->id]['role']=='student')
				{
					$division=$extended[$answer->id]['grade'];
				} elseif ($extended[$answer->id]['role']=='teacher')
				{
					$division=$extended[$answer->id]['role'];
				} else
				{
					$division='other';
				}
				if (!isset($legend[$division]))
				{
					$legend[$division]=$legend_count;
					$legend_count++;
				}
				//$raw[]=array($division, $answer->body);
				$raw[]=array($legend[$division], $answer->body);
				$traw[$legend[$division]][$answer->body] += 1;
				$location = empty($extended[$answer->id]['school']) ? 'other' : $extended[$answer->id]['school'];
				$diversity[$location.'_'.$division] = isset($diversity[$location.'_'.$division]) ? $diversity[$location.'_'.$division]+1 : 1;
				if (isset($profile[$answer->body]))
				{
					$profile[$answer->body] = $profile[$answer->body] + 1;
				}
				else
				{
					$profile[$answer->body] = $profile[$answer->body] + 1;
					$brick['global']['unique'] = isset($brick['global']['unique']) ? $brick['global']['unique'] + 1 : 1;
				}
				// Global Value
				$brick['global']['total'] = isset($brick['global']['total']) ? $brick['global']['total'] + $answer->body : $answer->body;
				$brick['global']['count'] = isset($brick['global']['count']) ? $brick['global']['count'] + 1 : 1;
				$brick['global']['average'] = $brick['global']['total'] / $brick['global']['count'];
				if (!empty($extended[$answer->id]['role'])) // Role Values
				{
					$role = ($extended[$answer->id]['role'] == 'student' || $extended[$answer->id]['role'] == 'teacher') ? $extended[$answer->id]['role'] : 'other';
					$brick[$role]['total'] = isset($brick[$role]['total']) ? $brick[$role]['total'] + $answer->body : $answer->body;
					$brick[$role]['count'] = isset($brick[$role]['count']) ? $brick[$role]['count'] + 1 : 1;
					$brick[$role]['average'] = $brick[$role]['total'] / $brick[$role]['count'];
				}
				// Grade Value
				if (!empty($extended[$answer->id]['grade']))
				{
					$brick['grades']['grade'.$extended[$answer->id]['grade']]['total'] = isset($brick['grades']['grade'.$extended[$answer->id]['grade']]['total']) ? $brick['grades']['grade'.$extended[$answer->id]['grade']]['total'] + $answer->body : $answer->body;
					$brick['grades']['grade'.$extended[$answer->id]['grade']]['count'] = isset($brick['grades']['grade'.$extended[$answer->id]['grade']]['count']) ? $brick['grades']['grade'.$extended[$answer->id]['grade']]['count'] + 1 : 1;
					$brick['grades']['grade'.$extended[$answer->id]['grade']]['average'] = $brick['grades']['grade'.$extended[$answer->id]['grade']]['total'] / $brick['grades']['grade'.$extended[$answer->id]['grade']]['count'];
				}
				// School Value
				if (!empty($extended[$answer->id]['school']))
				{
					$brick['schools'][$extended[$answer->id]['school']]['total'] = isset($brick['schools'][$extended[$answer->id]['school']]['total']) ? $brick['schools'][$extended[$answer->id]['school']]['total'] + $answer->body : $answer->body;
					$brick['schools'][$extended[$answer->id]['school']]['count'] = isset($brick['schools'][$extended[$answer->id]['school']]['count']) ? $brick['schools'][$extended[$answer->id]['school']]['count'] + 1 : 1;
					$brick['schools'][$extended[$answer->id]['school']]['average'] = $brick['schools'][$extended[$answer->id]['school']]['total'] / $brick['schools'][$extended[$answer->id]['school']]['count'];
				}
				/* OUT of scope for this test.
				// School/Grade Value
				$brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']]['total'] = isset($brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']]['total']) ? $brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']]['total'] + $answer->body : $answer->body;
				$brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']]['count'] = isset($brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']]['count']) ? $brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']]['count'] + 1 : 1;
				$brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']]['average'] = $brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']]['total'] / $brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']]['count'];
				// School/Grade/Teacher Value
				$brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']][$extended[$answer->id]['teacher']]['total'] = isset($brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']][$extended[$answer->id]['teacher']]['total']) ? $brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']][$extended[$answer->id]['teacher']]['total'] + $answer->body : $answer->body;
				$brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']][$extended[$answer->id]['teacher']]['count'] = isset($brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']][$extended[$answer->id]['teacher']]['count']) ? $brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']][$extended[$answer->id]['teacher']]['count'] + 1 : 1;
				$brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']][$extended[$answer->id]['teacher']]['average'] = $brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']][$extended[$answer->id]['teacher']]['total'] / $brick[$extended[$answer->id]['school']]['grade'.$extended[$answer->id]['grade']][$extended[$answer->id]['teacher']]['count'];
				*/
				// keeping it clean
				
				unset($extended[$answer->id]);
			}
        }
		// Extended Values
		// Independance
		$dups = max($profile);
		$brick['global']['independance'] = (1/pow(((($brick['global']['count']/$brick['global']['unique']/$dups))),$dups-1))/(1/($brick['global']['unique']/$brick['global']['count']));
		$tmp = array_keys($profile);
		$brick['global']['min']=min($tmp);
		$brick['global']['max']=max($tmp);
		$brick['global']['unique_average']=array_sum($tmp)/$brick['global']['unique'];
		$brick['global']['diversity']=count($diversity);
		$decentralization_average = array_sum($diversity)/count($diversity);
		$brick['global']['decentralization']=(1-(($decentralization_average/max($diversity))+(min($diversity)/max($diversity))/2))*100;
		$brick['global']['profile']=$profile;
		$brick['raw'] = $raw;
		foreach($traw as $gr=>$y)
		{
			foreach($y as $an=>$z)
			{
				$brick['raw_compressed'][]=array($gr, $an, $z);
			}
		}
		
		$brick['legend'] = array_flip($legend);
		$avg_range_low = $brick['global']['average']-($brick['global']['average']*.975);
		$avg_range_high = $brick['global']['average']+($brick['global']['average']*.975);
		$u_avg_range_low = $brick['global']['unique_average']-($brick['global']['unique_average']*.975);
		$u_avg_range_high = $brick['global']['unique_average']+($brick['global']['unique_average']*.975);
		$aft=0;
		$afc=0;
		$auft=0;
		$aufc=0;
		foreach ($profile as $value=>$cnt)
		{
			if ($value >= $avg_range_low && $value <= $avg_range_high){
					$aft = $aft + ($value*$cnt);
					$afc += $cnt;
			}
			if ($value >= $u_avg_range_low && $value <= $u_avg_range_high){
					$auft = $auft + $value;
					$aufc++;
			}
		}
		$brick['global']['filtered_average']=$aft/$afc;
		$brick['global']['filtered_unique_average']=$auft/$aufc;
		$brick['global']['filtered_average_count']= $afc;
		$brick['global']['filtered_average_total']= $aft;
		$brick['global']['filtered_unique_average_count']= $aufc;
		$brick['global']['filtered_unique_average_total']= $auft;
		$brick['global']['filtered_average_range']= $avg_range_low.' to '.$avg_range_high ;
		$brick['global']['filtered_unique_average_range']= $u_avg_range_low.' to '.$u_avg_range_high ;
		
		return $brick;
    }


    public function getAverageGuess()
    {
        $num_answers = 0;
        $total_answers = 0;
        foreach($this->_mixer->answers as $answer)
        {
            if (abs($answer->body)+1 > 1)
            {
                $num_answers++;
                $total_answers += $answer->body;
            }
        }
        $average = ($num_answers) ? $total_answers / $num_answers : 0 ;
        return $average;
    }

}