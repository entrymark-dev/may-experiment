<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 29/03/2015
 * Time: 2:19 PM
 */

class ComEntrymarkDomainEntityQuestion extends ComMediumDomainEntityMedium
{

    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'attributes' => array(
                'name'		=> array('required'=>false),
                'body'			=> array('required'=>AnDomain::VALUE_NOT_EMPTY),
                'params'=> array('required'=>false),
            ),
            'resources'		=> array('entrymark_questions'),
            'behaviors' => array('answerable')
        ));



        parent::_initialize($config);

        $behaviors = $config->behaviors->toArray();

        $key = array_search('subscribable', $behaviors, true);

        unset($config->behaviors->$key);

    }

    function getName()
    {
        return $this->getIdentifier()->name;
    }


    /**
     * To be tied into question params
     */
    function isNumeric()
    {
        $result = true;
        if($this->getQuestionParams()->type && $this->getQuestionParams()->type  != 'numeric')
        {
            $result = false;
        }
        return $result;
    }

// Should go to paramable behavior.
    function getQuestionParams()
    {

        $params = parent::getParams();

        if(is_string($params))
        {
            $params =  (array)json_decode($params);
            $params = new KConfig($params);
        }

        return $params;
    }

    public function __get($property)
    {

        $result = parent::__get($property);

        if(!$result)
        {
            $params = $this->getQuestionParams();

            if($params->$property)
            {
                $result = $params->$property;
            }
        }

        return $result;
    }
}