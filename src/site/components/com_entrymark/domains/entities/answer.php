<?php

/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 3/04/2015
 * Time: 10:15 PM
 */
class ComEntrymarkDomainEntityAnswer extends ComBaseDomainEntityNode
{

    function _initialize(KConfig $config)
    {
        $config->append(array(
            'inheritance' => array('abstract'=>$this->getIdentifier()->package == 'base'),
            'attributes'  => array(
                'body'			=> array('required'=>true),
                'params'=> array('required'=>false),
                'email' => array('required'=>false)
            ),
            'resources'		=> array('entrymark_answers'),
            'relationships' => array(

            ),
            'behaviors' => array(
                'parentable' => array('parent'=>'com://site/entrymark.domain.entity.question'),
                //'com://site/hashtags.domain.behavior.hashtagable',
				//'com://site/people.domain.behavior.mentionable',
				'modifiable',
				'authorizer',
				'locatable' ,
				'votable'
            )
        ));

        parent::_initialize($config);

    }

    function getAnswerParams()
    {

        $params = parent::getParams();

        if(is_string($params))
        {
            $params =  (array)json_decode($params);
            $params = new KConfig($params);
        }

        return $params;
    }

    public function __get($property)
    {

        $result = parent::__get($property);

        if(!$result)
        {
            $params = $this->getAnswerParams();

            if($params->$property)
            {
                $result = $params->$property;
            }
        }

        return $result;
    }
}