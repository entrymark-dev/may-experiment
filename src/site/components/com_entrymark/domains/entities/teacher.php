<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 17/04/2015
 * Time: 10:33 AM
 */

class ComEntrymarkDomainEntityTeacher extends ComBaseDomainEntityNode {

    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'inheritance' => array('abstract'=>$this->getIdentifier()->package == 'base'),
            'attributes' => array(
                'name'		=> array('required'=>AnDomain::VALUE_NOT_EMPTY, 'read'=>'public')
            ),
            'relationships'  => array(
               // 'author' => array('parent'=>'com:people.domain.entity.person', 'child_column'=>'created_by', 'required'=>true),
            ),
            'behaviors'	  => array(
                'votable',
                'authorizer',
                'privatable',
                'ownable',
                'dictionariable',
                'subscribable',
                'describable',
                'com://site/hashtags.domain.behavior.hashtagable'
            )

        ));

        parent::_initialize($config);
    }
}