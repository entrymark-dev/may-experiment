<?php
/**
 * @package      
 * @SubPackage
 * @copyright    Copyright (C) 2014 Greatwork.io. All rights reserved.
 * @license      No License
 * @link        http://greatwork.io
 */
class ComEntrymarkViewQuestionJson extends ComBaseViewJson
{

    protected function _getItem()
    {

        // todo: Clean this up. Too much copy and past from the parent. 
        if($item = $this->_state->getItem())
        {

            $item->isAnswerable();
            $results = $item->getResultsBrick();

            $item = $this->_serializeToArray($item);
            $item['results'] = $results;

            $commands = $this->getToolbarCommands('toolbar');

            if(!empty($commands))
                $item['commands'] = $commands;
        }

        return $item;
    }
}