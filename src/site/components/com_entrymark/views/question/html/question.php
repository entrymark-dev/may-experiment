<?php defined('KOOWA') or die('Restricted access') ?>
<?php @commands('toolbar') ?>

<div class="an-entity" style="display:none">
    <div class="clearfix">
        <div class="entity-portrait-square">
            <?= @avatar($question->author) ?>
        </div>

        <div class="entity-container">
            <div class="entity-author"><?= @name($question->author) ?></div>
            <div class="an-meta"><?//= @date($question->creationTime) ?></div>
        </div>
    </div>
</div>

<div class="entity-wrapper">
<h3 class="entity-title">
    <?=@escape($question->name) ?>
</h3>
<div class="entity-description">
    <?= @content($question->body) ?>
</div>

<form action="<?= @route('view=answer&question_id='. $question->id ) ?>" style="display: none">
<div class="entity-description">
    <input name="answer" type="text" value="" placeholder="<?= @text('COM-ENTRYMARK-ANSWER-PLACEHOLDER') ?>">

</div>
<div class="">
    <button data-trigger="Share"  class="btn btn-primary" >
        <?= @text('LIB-AN-ACTION-POST') ?>
    </button>
</div>
</form>

</div>
