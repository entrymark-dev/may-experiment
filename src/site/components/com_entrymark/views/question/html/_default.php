<?php defined('KOOWA') or die('Restricted access') ?>
<?php @commands('toolbar') ?>

<h1><?= $question->body ?></h1>

<div class="an-entity">
    <div class="clearfix">
        <div class="entity-portrait-square">
            <?= @avatar($question->author) ?>
        </div>

        <div class="entity-container">
            <h4 class="entity-author"><?= @name($question->author) ?></h4>
            <div class="an-meta"><?= @date($question->creationTime) ?></div>
        </div>
    </div>
</div>

<form action="<?= @route('view=answer&question_id='. $question->id ) ?>">
<div class="entity-description">
    <input name="answer" type="text" value="<?= @text('COM-ENTRYMARK-ANSWER-PLACEHOLDER') ?>">
</div>
<div class="">
    <button data-trigger="Share"  class="btn btn-primary" >
        <?= @text('LIB-AN-ACTION-POST') ?>
    </button>
</div>
</form>

