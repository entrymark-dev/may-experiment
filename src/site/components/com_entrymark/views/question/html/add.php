<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 29/03/2015
 * Time: 2:59 PM
 */
?>
<div class="row">
    <div class="span8">
        <?= @helper('ui.header', array()) ?>

        <div class="">
            <form class="question-form"
                  action="<?= @route('view=question&format=json&oid=' . $actor->id) ?>"
                  method="post" enctype="multipart/form-data">

                <div class="form-actions">

                    <button class="btn btn-primary"><?= @text('COM-ENTRYMARK-POST') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>