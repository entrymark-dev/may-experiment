<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 4/04/2015
 * Time: 12:14 PM
 */
?>
<div class="row">
	<div class="col-sm-8" id="question">
	    <?//= @helper('ui.header', array()) ?>
	    <?= @template('question') ?>
	    <?= @helper('ui.answers', $question, array('editor' => false)) ?>
	    <div class="comment-section" style="display: none">
	        <h4><?= @text('COM-ENTRYMARK-QUESTION-COMMENT-HEADER') ?></h4>
	        <?//= @helper('ui.comments', $question, array('editor' => true)) ?>
	    </div>
	</div>
	<div class="col-sm-3">
		<? if($question->image) :?>
	        <div class="entity-image">
	            <img src="<?= $question->image ?>" class="img img-responsive" />
	        </div>
	        <a href="<?= $question->image ?>" class="fresco fresco-link pull-right">View Larger Image</a>
	        <? if($question->external_url) :?>
	        <div class="entity-link">
	            <a href="<?= $question->external_url ?> &raquo;" class="pull-right">
	                <?= $question->external_url_text ? $question->external_url_text : 'Checkout what\'s next' ?>
	            </a>
	        </div>
	    <? endif; ?>
	    <? endif; ?>
   </div>
</div>