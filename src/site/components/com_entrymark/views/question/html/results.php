<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 8/05/2015
 * Time: 12:49 PM
 */
?>
<? $answers = $question->answers ; //gp($answers->count(), __METHOD__); //gp($question->getAverageGuess()); //gp($question->getResultsBrick())?>
<?
$results = $question->getResultsBrick()
?>


<div>
<h1>Results</h1>
<h2>Aggregation</h2>
<p> Aggregate Result: <?= $results['global']['average'] ?> </p>
<p> Filtered Aggregate Result: <?= $results['global']['filtered_average'] ?> </p>
<p> Participants: <?= $results['global']['count'] ?> </p>
<p> Unique Responses: <?= $results['global']['unique'] ?> </p>
<p> Unique Aggregate Result: <?= $results['global']['unique_average'] ?> </p>
<p> Filtered Unique Aggregate Result: <?= $results['global']['filtered_unique_average'] ?> </p>

<p> Highest Estimate: <?= $results['global']['max'] ?> </p>
<p> Lowest Estimate: <?= $results['global']['min'] ?> </p>
<h3>Indicators</h3>
<p> Independance Rating: <?= $results['global']['independance'] ?> (Lower is better) </p>
<p> Diversity: <?= $results['global']['diversity'] ?> (groups) </p>
<p> Decentralization: <?= $results['global']['decentralization'] ?>% (Higher is better) </p>
<br/>
<h2>Role Based Results</h2>
<? if (isset($results['teacher']['average'])) { ?>
	<h3>Teacher Results</h3>
	<p> Aggregate Result: <?= $results['teacher']['average'] ?> </p>
	<p> Participants: <?= $results['teacher']['count'] ?> </p>
	<p> Opinion Share: <?= ($results['teacher']['count']/$results['global']['count'])*100 ?>% </p>
	<br/>
<? } 
if (isset($results['student']['average'])) { ?>
	<h3>Student Results</h3>
	<p> Aggregate Result: <?= $results['student']['average'] ?> </p>
	<p> Participants: <?= $results['student']['count'] ?> </p>
	<p> Opinion Share: <?= ($results['student']['count']/$results['global']['count'])*100 ?>% </p>
	<br/>
<? }

if (isset($results['teacher']['average'])) { ?>

<h3>Other Results</h3>
<p> Aggregate Result: <?= $results['other']['average'] ?> </p>
<p> Participants: <?= $results['other']['count'] ?> </p>
<p> Opinion Share: <?= ($results['other']['count']/$results['global']['count'])*100 ?>% </p>
<br/>
<? }  ?>
<h2>School Results</h2>
<? foreach ($results['schools'] as $key=>$val) {

    $schoolName = @service('repos://site/entrymark.school')->fetch($key)->name;
    ?>
<p> <?= $schoolName ?> (<?= $key ?>) : Aggregate Result  <?= $val['average'] ?> with <?= $val['count'] ?> Participants and an Opinion Share of <?= ($val['count']/$results['global']['count'])*100 ?>%. </p>
<? } ?>
<br/>
<h2>Grade Results</h2>
<? foreach ($results['grades'] as $key=>$val) { ?>
<p> <?=$key ?> : Aggregate Result  <?= $val['average'] ?> with <?= $val['count'] ?> Participants and an Opinion Share of <?= ($val['count']/$results['global']['count'])*100 ?>%.</p>
<? } ?>
</div>
<pre>
<? print_r($results) ?>
</pre>

