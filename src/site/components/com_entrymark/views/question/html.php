<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 26/03/2015
 * Time: 9:24 AM
 */

class ComEntrymarkViewQuestionHtml extends ComBaseViewHtml
{

     function _layoutDefault()
    {
        $this->set('gadgets', new LibBaseTemplateObjectContainer());
        $this->set('composers', new LibBaseTemplateObjectContainer());

        $context = new KCommandContext();
        $context->actor	= $this->viewer;
        $context->gadgets = $this->gadgets;
        $context->composers = $this->composers;

        //make all the apps to listen to dispatcher
        $components = $this->getService('repos://site/components.component')->fetchSet();

        $components->registerEventDispatcher($this->getService('anahita:event.dispatcher'));

        $this->getService('anahita:event.dispatcher')->dispatchEvent('onDashboardDisplay', $context);
    }
}