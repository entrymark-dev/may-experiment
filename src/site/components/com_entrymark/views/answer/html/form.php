<?php defined('KOOWA') or die('Restricted access') ?>
<?php


$url 	= empty($answer) ?  $parent->getURL() : $answer->getURL();
$action = empty($answer)  ? 'addanswer' : 'editanswer';
$editor = !isset($editor) ? false : $editor;

$body = isset($answer) ? $answer->getBody() : '';
$answer = empty($answer) ? null: $answer;

$template = "form_case$case";

;
if ( $editor ) {
    $url .= '&answer[editor]=1';
}

?>

<form data-behavior="FormValidator"
      data-formvalidator-evaluate-Fields-on-change="false" data-formvalidator-evaluate-Fields-on-blur="false"
        class="an-comment-form form-horizontal" method="post" action="<?= @route($url) ?>">

		<input type="hidden" name="_action"    value="<?= $action ?>" />

		<?= @template($template, array('answer' => $answer, 'body' => $body)) ?>

</form>
