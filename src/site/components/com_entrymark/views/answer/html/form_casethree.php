<?php
/**
 * Created by PhpStorm.
 * User: GREG
 * Date: 14/05/2015
 * Time: 11:28 AM
 */
 //Form Data
 
$selections = array(
	array(	'qid'	=>			1,
			'text'  =>		   "WHEN THERE IS CONFLICT BETWEEN PEOPLE OVER IDEAS, I TEND TO FAVOR THE SIDE THAT:",
			'synthesist' =>    "Identifies and tried to bring out the conflict.",
			'idealist' =>      "Best expresses the ideals and values involved.",
			'pragmatist' =>    "Best reflects my personal opininons and experience.",
			'analyst' =>  	   "Approaches the situation with the most logic and consistency.",
			'realist' =>       "Expresses the argument most forcefully and concisely.",
	),
	array(	'qid'	=>			2,
			'text'  =>		   "WHEN I BEGIN WORK ON A GROUP PROJECT, WHAT IS MOST IMPORTANT TO ME IS:",
			'synthesist' =>    "Understanding the purposes and value of the project.",
			'idealist' =>      "Discovering the goals and values of individuals in the group.",
			'pragmatist' =>    "Determining how we are to go about doing the project.",
			'analyst' =>  	   "Understanding how the project can be of benefit to the group.",
			'realist' =>       "Getting the project organized and underway.",
	),
	array(	'qid'	=>			3,
			'text'  =>		   "GENERALLY SPEAKING, I ABSORB NEW IDEAS BEST BY:",
			'synthesist' =>    "Relating them to future or current activities.",
			'idealist' =>      "Applying them to concrete situations.",
			'pragmatist' =>    "Concentration and careful analysis.",
			'analyst' =>  	   "Understanding how they are similar to familiar ideas.",
			'realist' =>       "Contrasting them to other ideas.",
	),
	array(	'qid'	=>			4,
			'text'  =>		   "FOR ME, CHARTS AND GRAPHS IN A BOOK OR ARTICLE ARE USUALLY:",
			'synthesist' =>    "More useful than the narrative, if they are accurate.",
			'idealist' =>      "Useful, if they clearly display the important facts.",
			'pragmatist' =>    "Useful, if supported and explained by the narrative.",
			'analyst' =>  	   "Useful, if they raise questions about the narrative.",
			'realist' =>       "No more and no less useful than other material.",
	),
	array(	'qid'	=>			5,
			'text'  =>		   "IF I WERE ASKED TO DO A RESEARCH PROJECT, I WOULD PROBABLY START BY:",
			'synthesist' =>    "Trying to fit the project into a broad perspective.",
			'idealist' =>      "Deciding if I can do it alone or if I will need help.",
			'pragmatist' =>    "Speculating about what the possible outcomes might be.",
			'analyst' =>  	   "Determing whether or not the project should be done.",
			'realist' =>       "Trying to formulate the problem as thoroughly as possible.",
	),
	array(	'qid'	=>			6,
			'text'  =>		   "IF I WERE TO GATHER INFORMATION FOR NEIGHBORS ABOUT A COMMUNITY CONCERN, I WOULD PREFER TO:",
			'synthesist' =>    "Meet with them individually and ask specific questions.",
			'idealist' =>      "Hold an open meeting and ask them to air their views.",
			'pragmatist' =>    "Interview them in small groups and ask general questions.",
			'analyst' =>  	   "Meet informally with key people to get their ideas.",
			'realist' =>       "Ask them to bring me all the relevant information that they have.",
	),
	array(	'qid'	=>			7,
			'text'  =>		   "I AM LIKELY TO BELIEVE THAT SOMETHING IS TRUE IS IT:",
			'synthesist' =>    "Has held up against opposition.",
			'idealist' =>      "Fits with other things that I believe.",
			'pragmatist' =>    "Has been shown to hold up in proctice.",
			'analyst' =>  	   "Makes sense logically and scientifically.",
			'realist' =>       "Can be personally verified by observable facts.",
	),
	array(	'qid'	=>			8,
			'text'  =>		   "WHEN I READ A MAGAZINE ARTICLE IN MY LEISURE TIME, IT IS LIKELY TO BE ABOUT:",
			'synthesist' =>    "How someone resolved a personal or social problem.",
			'idealist' =>      "A controversial or political issue.",
			'pragmatist' =>    "An account of scientific or historical research.",
			'analyst' =>  	   "An interesting, humorous person or event.",
			'realist' =>       "A true account of someone's interesting experience.",
	),
	array(	'qid'	=>			9,
			'text'  =>		   "WHEN I READ A REPORT AT WORK, I AM LIKELY TO PAY THE MOST ATTENTION TO:",
			'synthesist' =>    "The relation of the conclusions to my own experience.",
			'idealist' =>      "Whether or not the recommendations can be accomplished.",
			'pragmatist' =>    "The validity of the findings, backed up by data.",
			'analyst' =>  	   "The writer's understanding of goals and objectives.",
			'realist' =>       "The inferences that are drawn from the data.",
	),
	array(	'qid'	=>			10,
			'text'  =>		   "WHEN I HAVE A TASK TO DO, THE FIRST THING I WANT TO KNOW IS:",
			'synthesist' =>    "What the best method is for getting the task done.",
			'idealist' =>      "Who wants the task done and when.",
			'pragmatist' =>    "Why the task is worth doing.",
			'analyst' =>  	   "What effect it may have on other tasks that have to be done.",
			'realist' =>       "What the immediate benefit is for doing the task.",
	),
	array(	'qid'	=>			11,
			'text'  =>		   "I USUALLY LEARN THE MOST ABOUT HOW TO DO SOMETHING NEW BY:",
			'synthesist' =>    "Understanding how it is related to other things I know.",
			'idealist' =>      "Starting in to practice it as soon as possible.",
			'pragmatist' =>    "Listening to differing views about how it is done.",
			'analyst' =>  	   "Having someone show me how to do it.",
			'realist' =>       "Analyzing how to do it the best way.",
	),
	array(	'qid'	=>			12,
			'text'  =>		   "IF I WERE TO BE TESTED OR EXAMINED, I WOULD PREFER:",
			'synthesist' =>    "An objective, problem oriented set of questions on the subject.",
			'idealist' =>      "A debate with others who are also being tested.",
			'pragmatist' =>    "An oral-visual presentation covering what I know.",
			'analyst' =>  	   "An informal report on how I have applied what I have learned.",
			'realist' =>       "A written report covering backround, theory, and method.",
	),
	array(	'qid'	=>			13,
			'text'  =>		   "PEOPLE WHOSE ABILITIES I RESPECT THE MOST ARE LIKELY TO BE:",
			'synthesist' =>    "Philosophers and statesmen.",
			'idealist' =>      "Writers and teachers.",
			'pragmatist' =>    "Business and government leaders.",
			'analyst' =>  	   "Economists and engineers.",
			'realist' =>       "Farmers and journalists.",
	),
	array(	'qid'	=>			14,
			'text'  =>		   "GENERALLY SPEAKING, I FIND A THEORY USEFUL IF IT:",
			'synthesist' =>    "Seems related to other theories or ideas that I have learned.",
			'idealist' =>      "Explains things to me in a new way.",
			'pragmatist' =>    "Can systematically explain a number of related situations.",
			'analyst' =>  	   "Serves to clarify my own experience and observations.",
			'realist' =>       "Has a practical and concrete application.",
	),
	array(	'qid'	=>			15,
			'text'  =>		   "WHEN I READ AN ARTICLE ON A CONTROVERSIAL SUBJECT, I PREFER	THAT IT:",
			'synthesist' =>    "Show the benefits to me for choosing a point of view.",
			'idealist' =>      "Set forth all the facts in the controversy.",
			'pragmatist' =>    "Logically outline the issues involved.",
			'analyst' =>  	   "Identify the values supported by the writer.",
			'realist' =>       "Highlight both sides of the issue and clarify the conflict.",
	),
	array(	'qid'	=>			16,
			'text'  =>		   "IF I READ A BOOK OUTSIDE MY FIELD, I AM MOST LIKELY TO DO SO BECAUSE OF:",
			'synthesist' =>    "An interest in improving my professional knowledge.",
			'idealist' =>      "Having been told it would be useful by someone I respect.",
			'pragmatist' =>    "A desire to extend my general knowledge.",
			'analyst' =>  	   "A desire to get outside my field for a change.",
			'realist' =>       "Curiosity to learn more about the specific subject.",
	),
	array(	'qid'	=>			17,
			'text'  =>		   "WHEN I FIRST APPROACH A TECHNICAL PROBLEM, I AM MOST LIKELY TO:",
			'synthesist' =>    "Try to relate it to a broader problem or theory.",
			'idealist' =>      "Look for ways to get the problem solved quickly.",
			'pragmatist' =>    "Think of a number of opposing ways to solve it.",
			'analyst' =>  	   "Look for ways that others might have solved it.",
			'realist' =>       "Try to find the best procedure for solving it.",
	),
	array(	'qid'	=>			18,
			'text'  =>		   "GENERALLY SPEAKING, I AM MOST INCLINED TO:",
			'synthesist' =>    "Find existing methods that work, and use them as well as possible.",
			'idealist' =>      "Speculate about how dissimilar methods might work together.",
			'pragmatist' =>    "Discover new and better methods.",
			'analyst' =>  	   "Find ways to make existing methods work in a new and better way.",
			'realist' =>       "Figure out how existing methods ought to work.",
	));
	if (isset($answer)) // Calculate the results
	{
		$synthesist = array_sum((array)$answer->synthesist);
		$idealist = array_sum((array)$answer->idealist);
		$pragmatist = array_sum((array)$answer->pragmatist);
		$analyst = array_sum((array)$answer->analyst);
		$realist = array_sum((array)$answer->realist);
	}
?>
<div class="comment-form-container">

	<? if (isset($answer)) { ?>
	<div class="entity-wrapper">
	<h4 class="entity-title">Your Results</h4>
		<div class="entity-description">
			<p>Synthesist: <?= $synthesist ?></p>
			<p>Idealist: <?= $idealist ?></p>
			<p>Pragmatist: <?= $pragmatist ?></p>
			<p>Analyst: <?= $analyst ?></p>
			<p>Realist: <?= $realist ?></p>
		</div>
	</div>
	<? } ?>
    <div class="form-group">

        <label for="person" class="col-sm-2 control-label">Group</label>
        <input required type="text" value="<?= $body ?>" name="body" id="person"/>

    </div>

    <div class="answer-section">

        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Date") ?></label>
            <div class="col-sm-6">
                <input required type="date" value="<?= ( isset($answer) && $answer->date) ? $answer->date : ''  ?>" name="params[date]"/>
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Your Email") ?></label>
            <div class="col-sm-6">
                <input
                 <? if(get_viewer()->id == 0): ?>
                    required
                 <? endif ?>
                    type="email"
                    value="<?= isset($answer) && $answer->email ? $answer->email : get_viewer()->email ?>"
                    name="email"/>
            </div>
        </div>
        <?
		foreach($selections as $elm)
		{
		?>
		<div class="form-group">
            <p><?= $elm['qid'].". ".@text($elm['text']) ?></p>
            <div class="col-sm-11">
			<table>
				<tr>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th></th>
				</tr>
				<tr>
					<td><input class="radio-rank" type="radio" name="params[synthesist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="1" value="5" 
						<?=( isset($answer) && $answer->synthesist->$elm['qid'] == 5) ? 'checked' : ''  ?><?= !isset($answer) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[synthesist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="2" value="4" 
						<?=( isset($answer) && $answer->synthesist->$elm['qid'] == 4) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[synthesist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="3" value="3" 
						<?=( isset($answer) && $answer->synthesist->$elm['qid'] == 3) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[synthesist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="4" value="2" 
						<?=( isset($answer) && $answer->synthesist->$elm['qid'] == 2) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[synthesist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="5" value="1" 
						<?=( isset($answer) && $answer->synthesist->$elm['qid'] == 1) ? 'checked' : ''  ?>>
					</td>
					<td><?=  $elm['synthesist'] ?></td>
				</tr>
				<tr>
					<td><input class="radio-rank" type="radio" name="params[idealist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="1" value="5" 
						<?=( isset($answer) && $answer->idealist->$elm['qid'] == 5) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[idealist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="2" value="4" 
						<?=( isset($answer) && $answer->idealist->$elm['qid'] == 4) ? 'checked' : ''  ?><?= !isset($answer) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[idealist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="3" value="3" 
						<?=( isset($answer) && $answer->idealist->$elm['qid'] == 3) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[idealist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="4" value="2" 
						<?=( isset($answer) && $answer->idealist->$elm['qid'] == 2) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[idealist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="5" value="1" 
						<?=( isset($answer) && $answer->idealist->$elm['qid'] == 1) ? 'checked' : ''  ?>>
					</td>
					<td><?=  $elm['idealist'] ?></td>
				</tr>
				<tr>
					<td><input class="radio-rank" type="radio" name="params[pragmatist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="1" value="5" 
						<?=( isset($answer) && $answer->pragmatist->$elm['qid'] == 5) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[pragmatist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="2" value="4" 
						<?=( isset($answer) && $answer->pragmatist->$elm['qid'] == 4) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[pragmatist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="3" value="3" 
						<?=( isset($answer) && $answer->pragmatist->$elm['qid'] == 3) ? 'checked' : ''  ?><?= !isset($answer) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[pragmatist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="4" value="2" 
						<?=( isset($answer) && $answer->pragmatist->$elm['qid'] == 2) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[pragmatist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="5" value="1" 
						<?=( isset($answer) && $answer->pragmatist->$elm['qid'] == 1) ? 'checked' : ''  ?>>
					</td>
					<td><?=  $elm['pragmatist'] ?></td>
				</tr>
				<tr>
					<td><input class="radio-rank" type="radio" name="params[analyst][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="1" value="5" 
						<?=( isset($answer) && $answer->analyst->$elm['qid'] == 5) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[analyst][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="2" value="4" 
						<?=( isset($answer) && $answer->analyst->$elm['qid'] == 4) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[analyst][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="3" value="3" 
						<?=( isset($answer) && $answer->analyst->$elm['qid'] == 3) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[analyst][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="4" value="2" 
						<?=( isset($answer) && $answer->analyst->$elm['qid'] == 2) ? 'checked' : ''  ?><?= !isset($answer) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[analyst][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="5" value="1" 
						<?=( isset($answer) && $answer->analyst->$elm['qid'] == 1) ? 'checked' : ''  ?>>
					</td>
					<td><?=  $elm['analyst'] ?></td>
				</tr>
				<tr>
					<td><input class="radio-rank" type="radio" name="params[realist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="1" value="5" 
						<?=( isset($answer) && $answer->realist->$elm['qid'] == 5) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[realist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="2" value="4" 
						<?=( isset($answer) && $answer->realist->$elm['qid'] == 4) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[realist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="3" value="3" 
						<?=( isset($answer) && $answer->realist->$elm['qid'] == 3) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[realist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="4" value="2" 
						<?=( isset($answer) && $answer->realist->$elm['qid'] == 2) ? 'checked' : ''  ?>>
					</td>
					<td><input class="radio-rank" type="radio" name="params[realist][<?= $elm['qid'] ?>]" data-set="<?= $elm['qid'] ?>"  data-oldVal="0" data-col="5" value="1" 
						<?=( isset($answer) && $answer->realist->$elm['qid'] == 1) ? 'checked' : ''  ?><?= !isset($answer) ? 'checked' : ''  ?>>
					</td>
					<td><?=  $elm['realist'] ?></td>
				</tr>
			</table>
            </div>
        </div>
        <?
		}
		?>
		
		<div class="form-group" style="margin-top:30px">
            <label for="inputPassword3" class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-9">
                <?php if(isset($answer)) : ?>
                    <button data-trigger="Request"  type="button" class="btn btn-primary"  name="cancel"  data-request-options="{method:'get',url:'<?=@route($answer->getURL().'&answer[layout]=list&answer[editor]='.$editor)?>',replace:this.getParent('form')}"><?= @text('LIB-AN-ACTION-CANCEL') ?></button>
                    <button data-trigger="Answer"  data-request-options="{replace:this.getParent('form')}" type="submit" class="btn btn-primary"   name="submit">Submit<?//= @text('LIB-AN-ACTION-POST') ?></button>
                <?php else : ?>
                    <button data-trigger="Answer"  data-request-options="{onSuccess:function(){this.form.getElement('textarea').value=''}.bind(this),inject:{where:'bottom',element:this.getParent('.an-comments-wrapper').getElement('.an-comments')}}" type="submit" class="btn btn-primary">Submit<?//= @text('LIB-AN-ACTION-POST') ?></button>
                <?php endif; ?>
            </div>
        </div>
    </div>
	<script>
		$(function() {
			var col, el, set, old_col, ref_name;
			
			$("input[type=radio].radio-rank").change(function() {
			   el = $(this);
			   col = el.data("col");
			   set = el.data("set");
			   old_col = 0;
				$("[name='"+el.attr('name')+"']").each(function( i ) {
					if ( $(this).attr("oldval") > 0 ) {
					  old_col = $(this).attr("oldval");
					}
				});
			   $(" input[data-set="+ set +"][data-col="+ col +"]").each(function( i ) {
					if ( $(this).prop("checked") == true ) {
					  ref_name=$(this).attr("name");
					  $(this).prop("checked", false);
					  $(" input[data-set="+ set +"][data-col="+ old_col +"][name='"+ref_name+"']").prop("checked", true);
					}
				});
			   el.prop("checked", true);
			   oldvals();
			});
		});
		
		var oldvals = function() {
			$("input[type=radio].radio-rank").each(function( i ) {
				if ( $(this).prop("checked") ) {
				  $(this).attr('oldval',  $(this).data("col") );
				} else {
				  $(this).attr('oldval',  0 );
				  
				}
			  });
		};
		
		$(document).ready(function(){
		   oldvals();
		});
	</script>
