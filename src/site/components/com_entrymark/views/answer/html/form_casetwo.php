<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 11/05/2015
 * Time: 2:28 PM
 */
?>
<div class="comment-form-container">

    <div class="form-group">

        <label for="person" class="col-sm-2 control-label">Person</label>
        <input required type="text" value="<?= $body ?>" name="body" id="person"/>

    </div>

    <div class="answer-section">

        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Date") ?></label>
            <div class="col-sm-9">
                <input required type="date" value="<?= isset($answer) && $answer->date ?>" name="params[date]"/>
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Location") ?></label>
            <div class="col-sm-9">
                <?= @helper('selector.map', array('name' => 'params[location]')) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Your Email") ?></label>
            <div class="col-sm-9">
                <input
                 <? if(get_viewer()->id == 0): ?>
                    required
                 <? endif ?>
                    type="email"
                    value="<?= isset($answer) && $answer->email ? $answer->email : get_viewer()->email ?>"
                    name="email"/>
            </div>
        </div>
        <div class="form-group" style="margin-top:30px">
            <label for="inputPassword3" class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-9">
                <?php if(isset($answer)) : ?>
                    <button data-trigger="Request"  type="button" class="btn btn-primary"  name="cancel"  data-request-options="{method:'get',url:'<?=@route($answer->getURL().'&answer[layout]=list&answer[editor]='.$editor)?>',replace:this.getParent('form')}"><?= @text('LIB-AN-ACTION-CANCEL') ?></button>
                    <button data-trigger="Answer"  data-request-options="{replace:this.getParent('form')}" type="submit" class="btn btn-primary"   name="submit">Submit<?//= @text('LIB-AN-ACTION-POST') ?></button>
                <?php else : ?>
                    <button data-trigger="Answer"  data-request-options="{onSuccess:function(){this.form.getElement('textarea').value=''}.bind(this),inject:{where:'bottom',element:this.getParent('.an-comments-wrapper').getElement('.an-comments')}}" type="submit" class="btn btn-primary">Submit<?//= @text('LIB-AN-ACTION-POST') ?></button>
                <?php endif; ?>
            </div>
        </div>
    </div>