<?php
/**
 * Created by PhpStorm.
 * User: CAMERON
 * Date: 11/05/2015
 * Time: 2:28 PM
 */

?>
<div class="comment-form-container">

    <div class="form-group">
        <?php if ( $editor ) : ?>
            <?php $id   = isset($answer) ? $answer->id   : time() ?>
            <?php $body = isset($answer) ? $answer->getBody() : ''?>
            <?= @editor(array('name'=>'body', 'content'=>$body, 'html'=>array('cols'=>50, 'rows'=>5, 'class'=>'input-block-level','data-validators'=>'required maxLength:5000', 'id'=>'an-comment-body-'.$id)))?>
        <?php else : ?>

            <? if($parent->isNumeric()) :?>
                <label for="number" class="col-sm-2 control-label answer-label">Answer<br /><i style="font-weight:normal;">In Kilograms</i><?//= @text('COM-ENTRYMARK-ANSWER-THE-QUESTION') ?></label>
                <div class="col-sm-8">
                    <input required type="number" value="<?= $body ?>" name="body"/>
                </div>
            <?php else: ?>
                <textarea name="body" cols="50" rows="3" class="input-block-level" data-validators="required maxLength:5000"><?= isset($answer) ? $answer->getBody() : '' ?></textarea>
            <?php endif;?>
        <?php endif;?>
    </div>

    <div class="answer-section">
        <label class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-sm-6 form-note">
            The fields below are all optional. Filling them out will help us see who had the best guesses.
        </div>
        <div class="clr"></div>
        <div class="form-group" id="role_radio">
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Teacher or Student?") ?></label>
            <div class="col-sm-8">
                <?= @helper('selector.role',
                    array('name' => 'params[role]',
                        'options' => array(
                            array('value' => 'teacher', 'label' => 'Teacher'),
                            array( 'value' => 'student','label' => 'Student'),
                            array( 'value' => '', 'label' => 'Other')
                        ),
                        //'attribs' => array('id'=>'role_radio'),
                        'selected' => !empty($answer) && isset($answer->role) ? $answer->role : 'student'/*$answer->grade*/)
                ) ?>
                <input disabled name="role" value="" type="text" id="other_role" style="display: none"/>
            </div>
        </div>


        <div class="form-group" id="teacher_name" >
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Your Teacher") ?></label>
            <div class="col-sm-8">
                <input type="text" value="<?= isset($answer) && $answer->teacher ? $answer->teacher : '' ?>" name="teacher"/>
            </div>
        </div>

        <div class="form-group" id="email">
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Your Email or Name") ?></label>
            <div class="col-sm-8">
                <input type="text" value="<?= isset($answer) && $answer->email ? $answer->email : get_viewer()->email ?>" name="email"/>
            </div>

        </div>


        <div class="form-group" id="grade">
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Your Grade") ?></label>
            <div class="col-sm-8">
                <?= @helper('selector.grade', array('name' => 'params[grade]', 'prompt' => 'select your grade', 'selected' => /*$answer->grade*/"")) ?>
            </div>
        </div>

        <div class="form-group" id="school">
            <label for="inputPassword3" class="col-sm-2 control-label"><?= @text("Your School") ?></label>
            <div class="col-sm-8">
                <?= @helper('selector.school', array('name' => 'params[school]', 'prompt' => 'select your school', 'selected' => /*$answer->school*/"")) ?>
            </div>
        </div>

        <div class="form-group" style="margin-top:30px">
            <label for="inputPassword3" class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-8">
                <?php if(isset($answer)) : ?>
                    <button data-trigger="Request"  type="button" class="btn btn-primary"  name="cancel"  data-request-options="{method:'get',url:'<?=@route($answer->getURL().'&answer[layout]=list&answer[editor]='.$editor)?>',replace:this.getParent('form')}"><?= @text('LIB-AN-ACTION-CANCEL') ?></button>
                    <button data-trigger="Answer"  data-request-options="{replace:this.getParent('form')}" type="submit" class="btn btn-primary"   name="submit">Submit<?//= @text('LIB-AN-ACTION-POST') ?></button>
                <?php else : ?>
                    <button data-trigger="Answer"  data-request-options="{onSuccess:function(){this.form.getElement('textarea').value=''}.bind(this),inject:{where:'bottom',element:this.getParent('.an-comments-wrapper').getElement('.an-comments')}}" type="submit" class="btn btn-primary">Submit<?//= @text('LIB-AN-ACTION-POST') ?></button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
    <script inline>
        jQuery(document).ready(function(){

            jQuery("#role_radio input:radio").change(
                function() {
                    jQuery("#teacher_name, #other_role, #grade").hide();
                }

            );


            jQuery('#role_radio input[value=""]').change(function(){
                if(this.checked) {
                    $("#grade").hide();
                    $("#other_role").show().attr('disabled', false);
                    $("#teacher_name").hide();
                    $("#school").hide()
                }
            });
            jQuery('#role_radio input[value="student"]').change(function(){
                var textInput = $("#teacher_name");

                if(this.checked) {
                    textInput.find('input:text').focus();
                    textInput.show();
                    $("#grade").show();
                    $("#other_role").hide().attr('disabled', true);
                    $("#school").show()
                } else {
                    textInput.hide();
                    $("#grade").hide();
                    textInput.find('input:text').val('');

                }
            })

            jQuery('#role_radio input[value="teacher"]').change(function(){

                if(this.checked) {
                    $("#grade").hide();
                    $("#teacher_name").hide()
                    $("#email").show();
                    $("#school").show()
                    $("#other_role").hide().attr('disabled', true);
                } else {
                    $("#grade").hide();
                    $("#teacher_name").show()
                    //$("#email").hide();
                }
            })

        });
    </script>