<?php if(defined('JDEBUG') && JDEBUG ) : ?>
	<?php @helper('javascript.combine', array(
  				'file'   => JPATH_ROOT.'/media/lib_anahita/js/site.js',
  				'output' => JPATH_ROOT.'/media/lib_anahita/js/production/site.uncompressed.js'
  			)) ?>
<script src="media://lib_anahita/js/production/site.uncompressed.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<?php else : ?>
<script src="media://lib_anahita/js/production/site.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<?php endif; ?>
