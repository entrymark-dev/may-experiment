<?php defined('KOOWA') or die;?>
<!DOCTYPE html>
<html>
	<head>
  		<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	    <?= @render('favicon') ?>
  	    <?= @render('style') ?>
  	    <link rel="stylesheet" href="base://templates/entrymark/css/style1/temp.css" type="text/css" />
  	    <link rel="stylesheet" href="base://templates/entrymark/css/fresco/fresco.css" type="text/css" />
  	    <?= @template('tmpl/js') ?>
  	    <script type="text/javascript" src="base://templates/entrymark/js/fresco.js"></script>
        <?//= @render('analytics') ?>
  	</head>
    <body>	  	
        <div id="container-system-message" class="container">
        	<?= @render('messages') ?>
        </div>    		
        
        <?= @template('tmpl/navbar') ?>
        
        <div class="container">
        <?= $this->getView()->content; ?>
        </div>

    </body>
</html>
