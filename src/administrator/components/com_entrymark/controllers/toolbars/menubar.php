<?php

 class ComEntrymarkControllerToolbarMenubar extends ComBaseControllerToolbarMenubar
 {     
     /**
      * Add subscription menu bar
      * .
      * @param KEvent $event The event object
      * 1
      * @return void
      */
     public function onAfterControllerBrowse(KEvent $event)
     {     
        
     	$menu = array(
            'schools'   => JText::_('Schools'),
            'teachers' => JText::_('Teachers'),
            'questions' => JText::_('Questions'),
            'answers' => JText::_('Answers')

         );

         foreach($menu as $view => $label)
         {
         	$this->addCommand( $label, array(	
         		'active'=> ($view == $this->getController()->view), 
         		'href'=>JRoute::_('index.php?option=com_entrymark&view='.$view
         	)));
         }       
     }     
 }