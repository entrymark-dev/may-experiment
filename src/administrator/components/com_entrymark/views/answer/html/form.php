<?php defined('KOOWA') or die('Restricted access'); ?>

<script>

</script>
<form data-behavior="FormValidator" action="<?= @route($answer->persisted() ? array('id'=>$answer->id) : '')?>" method="post" class="-koowa-form">
    <div class="col width-50">

        <fieldset class="adminform">
            <legend><?= JText::_( 'answer' ); ?></legend>

            <table class="admintable">

                <tr>
                    <td width="100" align="right" class="key">
                        <?= @text('Answer') ?>
                    </td>
                    <td>
                        <input type="text" value="<?=$answer->body ?>" size=45 name="body" />
                    </td>
                </tr>

            </table>

        </fieldset>

    </div>
</form>