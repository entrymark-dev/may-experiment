<? defined('KOOWA') or die('Restricted access'); ?>

<? $i = 0; $m = 0; ?>
<? foreach ($questions as $question) : ?>
    <tr class="<?php echo 'row'.$m; ?>">
        <td align="center"><?= $i + 1; ?></td>
        <td align="center"><?= @helper('grid.checkbox', array('row'=>$question)); ?></td>
        <td>
            <a href="<?= @route('view=question&id='.$question->id.'&hidemainmenu=1')?>">
                <?= $question->body ?>
            </a>
        </td>
<!--        <td align="center">--><?//= $question->discount * 100?><!--%</td>-->
<!--        <td align="center">--><?//= $question->limit ?><!-- </td>-->

        <td align="center"><?= $question->id; ?></td>
    </tr>
    <? $i = $i + 1; $m = (1 - $m); ?>
<? endforeach; ?>