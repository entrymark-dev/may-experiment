<?php defined('KOOWA') or die('Restricted access'); ?>

<script>

</script>
<form data-behavior="FormValidator" action="<?= @route($teacher->persisted() ? array('id'=>$teacher->id) : '')?>" method="post" class="-koowa-form">
    <div class="col width-50">

        <fieldset class="adminform">
            <legend><?= JText::_( 'Teacher' ); ?></legend>

            <table class="admintable">

                <tr>
                    <td width="100" align="right" class="key">
                        <?= @text('Name') ?>
                    </td>
                    <td>
                        <input type="text" value="<?=$teacher->name ?>" size="45" name="name" />
                    </td>
                    <td width="100" align="right" class="key">
                        <?= @text('School') ?>
                    </td>
                    <td>
                        <?= @helper('selector.school', array('name' => 'school', 'prompt' => 'select a school')) ?>
                    </td>
                </tr>

            </table>

        </fieldset>

    </div>


</form>