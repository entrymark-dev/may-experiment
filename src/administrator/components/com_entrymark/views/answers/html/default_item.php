<? defined('KOOWA') or die('Restricted access'); ?>

<? $i = 0; $m = 0; ?>
<? foreach ($answers as $answer) : ?>
    <tr class="<?php echo 'row'.$m; ?>">
        <td align="center"><?= $i + 1; ?></td>
        <td align="center"><?= @helper('grid.checkbox', array('row'=>$answer)); ?></td>
        <td>
            <a href="<?= @route('view=answer&id='.$answer->id.'&hidemainmenu=1')?>">
                <?= $answer->body ?>
            </a>
        </td>
<!--        <td align="center">--><?//= $answer->discount * 100?><!--%</td>-->
<!--        <td align="center">--><?//= $answer->limit ?><!-- </td>-->

        <td align="center"><?= $answer->id; ?></td>
    </tr>
    <? $i = $i + 1; $m = (1 - $m); ?>
<? endforeach; ?>