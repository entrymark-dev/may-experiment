<?php defined('KOOWA') or die('Restricted access'); ?>

<form data-behavior="FormValidator" action="<?= @route($question->persisted() ? array('id'=>$question->id) : '')?>" method="post" class="-koowa-form">
    <div class="col width-50">

        <fieldset class="adminform">
            <legend><?= JText::_( 'Question' ); ?></legend>

            <table class="admintable">

                <tr>
                    <td width="100" align="right" class="key">
                        <?= @text('Name') ?>
                    </td>
                    <td>
                        <input type="text" value="<?=$question->name ?>" size=45 name="name" />
                    </td>
                </tr>

            </table>

        </fieldset>

    </div>


</form>