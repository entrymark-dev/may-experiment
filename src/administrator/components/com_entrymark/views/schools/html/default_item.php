<? defined('KOOWA') or die('Restricted access'); ?>

<? $i = 0; $m = 0; ?>
<? foreach ($schools as $school) : ?>
    <tr class="<?php echo 'row'.$m; ?>">
        <td align="center"><?= $i + 1; ?></td>
        <td align="center"><?= @helper('grid.checkbox', array('row'=>$school)); ?></td>
        <td>
            <a href="<?= @route('view=school&id='.$school->id.'&hidemainmenu=1')?>">
                <?= $school->name ?>
            </a>
        </td>
<!--        <td align="center">--><?//= $school->discount * 100?><!--%</td>-->
<!--        <td align="center">--><?//= $school->limit ?><!-- </td>-->

        <td align="center"><?= $school->id; ?></td>
    </tr>
    <? $i = $i + 1; $m = (1 - $m); ?>
<? endforeach; ?>