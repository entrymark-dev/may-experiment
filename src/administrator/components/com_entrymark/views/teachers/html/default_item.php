<? defined('KOOWA') or die('Restricted access'); ?>

<? $i = 0; $m = 0; ?>
<? foreach ($teachers as $teacher) : ?>
    <tr class="<?php echo 'row'.$m; ?>">
        <td align="center"><?= $i + 1; ?></td>
        <td align="center"><?= @helper('grid.checkbox', array('row'=>$teacher)); ?></td>
        <td>
            <a href="<?= @route('view=teacher&id='.$teacher->id.'&hidemainmenu=1')?>">
                <?= $teacher->name ?>
            </a>
        </td>
        <td align="left"><?= $this->getService('repos://site/entrymark.school')->fetch($teacher->school)->name ?></td>
<!--        <td align="center">--><?//= $teacher->limit ?><!-- </td>-->

        <td align="center"><?= $teacher->id; ?></td>
    </tr>
    <? $i = $i + 1; $m = (1 - $m); ?>
<? endforeach; ?>