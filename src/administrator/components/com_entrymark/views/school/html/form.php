<?php defined('KOOWA') or die('Restricted access'); ?>

<script>

</script>
<form data-behavior="FormValidator" action="<?= @route($school->persisted() ? array('id'=>$school->id) : '')?>" method="post" class="-koowa-form">
    <div class="col width-50">

        <fieldset class="adminform">
            <legend><?= JText::_( 'School' ); ?></legend>

            <table class="admintable">

                <tr>
                    <td width="100" align="right" class="key">
                        <?= @text('Name') ?>
                    </td>
                    <td>
                        <input type="text" value="<?=$school->name ?>" size=45 name="name" />
                    </td>
                </tr>

            </table>

        </fieldset>

    </div>


</form>