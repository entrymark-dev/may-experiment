# README #

This is an Anahita package, that we are using to research collective intelligence . 

### What is this repository for? ###

* To provide for an experiment to test collective intelligence.

### How do I get set up? ###

* Set up a dummy database
* Follow the instructions at https://github.com/anahitasocial/anahita/wiki/Getting-Started
* Change your configuration.php file to use 'anahita' database. If you are working locally, also change the host and db credentials. 
* cd ./packages/; clone git@bitbucket.org:entrymark-dev/may-experiment.git entrymark;
* php anahita package:install entrymark
* configure apache with your new domain.

### ALL CHANGES ARE MADE IN THE ./packages/entrymark/ folder. ###